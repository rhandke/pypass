import click
from rofi import Rofi

from pypass.reader import PassReader
from pypass import gui
from pypass import password


@click.command()
@click.argument("store", type=click.Path(exists=True))
def cli(store) -> None:
    """Select password from STORE and type it to active window"""
    password_reader = PassReader(store)
    password_list = password_reader.get_passwords()
    pass_name = gui.select(Rofi(), password_list)
    if not pass_name:
        exit(0)
    gui.type(password.decrypt(pass_name))
