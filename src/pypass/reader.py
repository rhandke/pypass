from pathlib import Path
from typing import List


class PassReader:
    store: str
    password_list: List[Path]

    def __init__(self, store: str) -> None:
        if not Path(store).exists():
            raise FileNotFoundError
        self.store = store
        self.password_list = self._get_password_list(store)

    def get_passwords(self) -> List[str]:
        return [
            str(pw).replace(self.store, "").replace(".gpg", "").strip("/")
            for pw in self.password_list
        ]

    def _get_password_list(self, store: str) -> List[Path]:
        return [file for file in Path(store).glob("**/*.gpg")]
