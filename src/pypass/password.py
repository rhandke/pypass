import subprocess


def decrypt(password: str):
    pw = subprocess.run(["pass", "show", password], capture_output=True)

    if pw.returncode != 0:
        raise ValueError("Could not retrieve password.")

    return pw.stdout.decode().split("---")[0].replace("\\\\", "\\").strip("\n")
