import subprocess
from typing import List
from rofi import Rofi


def select(rofi: Rofi, items: List[str]) -> str:
    index, _ = rofi.select("Choose Password", items)
    if index == -1:
        return ""
    return str(items[index])


def type(item: str) -> None:
    subprocess.run(["xdotool", "type", item])
