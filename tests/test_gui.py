import subprocess
from unittest.mock import Mock
from _pytest.monkeypatch import MonkeyPatch
from pypass.gui import select, type


class MockRofi:
    def select(self, prompt, options, message="", select=None, **kwargs):
        return 1, 0


class MockRofiAbort:
    def select(self, prompt, options, message="", select=None, **kwargs):
        return -1, -1


def test_select() -> None:
    pws = ["dir1/pw1", "dir2/dir3/pw2", "pw3"]
    assert select(MockRofi(), pws) == "dir2/dir3/pw2"  # type: ignore


def test_select_abort() -> None:
    assert select(MockRofiAbort(), []) == ""  # type: ignore


def test_type_calls_xdotool_type(monkeypatch: MonkeyPatch) -> None:
    run_mock = Mock()
    monkeypatch.setattr(subprocess, "run", run_mock)
    type("teststring")
    run_mock.assert_called_once()
    run_mock.assert_called_with(["xdotool", "type", "teststring"])
