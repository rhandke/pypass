from pathlib import Path

import pytest

from pypass.reader import PassReader


@pytest.fixture
def password_store(tmp_path) -> Path:
    d: Path = tmp_path / "password_store"
    d.mkdir()

    a = d / "dir1"
    a.mkdir()
    b = d / "dir2"
    b.mkdir()
    pw1 = d / "pw1.gpg"
    pw1.touch()
    pw2 = a / "pw2.gpg"
    pw2.touch()
    pw3 = b / "pw3.gpg"
    pw3.touch()

    return d


def test_no_store() -> None:
    with pytest.raises(FileNotFoundError):
        PassReader("no_such_file")


def test_get_passwords(password_store: Path) -> None:
    pr = PassReader(str(password_store))
    result = pr.get_passwords()
    assert "pw1" in result
    assert "dir1/pw2" in result
    assert "dir2/pw3" in result
