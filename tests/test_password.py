import subprocess

import pytest
from pytest import MonkeyPatch

from pypass.password import decrypt


def test_decrypt(monkeypatch: MonkeyPatch) -> None:
    class Mock_Result:
        returncode = 0
        stdout = b"password"

    def mock_run(*args, **kwargs):
        return Mock_Result()

    monkeypatch.setattr(subprocess, "run", mock_run)

    assert decrypt("pypass") == "password"


def test_decrypt_no_pass(monkeypatch: MonkeyPatch) -> None:
    class Mock_Result:
        returncode = 1

    def mock_run(*args, **kwargs):
        return Mock_Result()

    monkeypatch.setattr(subprocess, "run", mock_run)

    with pytest.raises(ValueError):
        decrypt("doesnotexist")


def test_decrypt_escape_char(monkeypatch: MonkeyPatch) -> None:
    class Mock_Result:
        returncode = 0
        stdout = b"pass\\\\word"

    def mock_run(*args, **kwargs):
        return Mock_Result()

    monkeypatch.setattr(subprocess, "run", mock_run)

    assert decrypt("pypass") == "pass\\word"
