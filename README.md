# PyPass

A Rofi integration for GNU Pass written in Python.
Select a password from the GNU Pass password store and type it to the active window.

# Installation
Simply clone the repository and install the CLI.

```bash
git clone <tbd>
pip3 install .
```

# Usage
The `pypass` command expects the path to the password-store directory.

```bash
pypass <PATH-TO-PASSWORD-STORE>
```

This will pipe all passwords from the password-store directory into Rofi. After selecting a password, it will be typed to the active window using XDOTOOL TYPE.
